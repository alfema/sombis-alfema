%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: ApuntarMask
  m_Mask: 01000000010000000000000000000000000000000100000000000000010000000000000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Base Human
    m_Weight: 0
  - m_Path: Base HumanHeadGizmo
    m_Weight: 0
  - m_Path: Base HumanLCollarboneGizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger11Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger12Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger13Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger21Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger22Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger23Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger31Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger32Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFinger33Gizmo
    m_Weight: 0
  - m_Path: Base HumanLFootGizmo
    m_Weight: 0
  - m_Path: Base HumanLForearmGizmo
    m_Weight: 0
  - m_Path: Base HumanLPalmGizmo
    m_Weight: 0
  - m_Path: Base HumanLPlatform
    m_Weight: 0
  - m_Path: Base HumanLToe11Gizmo
    m_Weight: 0
  - m_Path: Base HumanLUpperarmGizmo
    m_Weight: 0
  - m_Path: Base HumanPelvis
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanLThigh
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanLThigh/Base HumanLCalf
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanLThigh/Base HumanLCalf/Base HumanLFoot
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanLThigh/Base HumanLCalf/Base HumanLFoot/Base
      HumanLToe11
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanLThigh/Base HumanLCalf/Base HumanLFoot/Base
      HumanLToe11/Base HumanLToe12
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanRThigh
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanRThigh/Base HumanRCalf
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanRThigh/Base HumanRCalf/Base HumanRFoot
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanRThigh/Base HumanRCalf/Base HumanRFoot/Base
      HumanRToe11
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanRThigh/Base HumanRCalf/Base HumanRFoot/Base
      HumanRToe11/Base HumanRToe12
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger11
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger11/Base HumanLFinger12
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger11/Base HumanLFinger12/Base HumanLFinger13
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger11/Base HumanLFinger12/Base HumanLFinger13/Base
      HumanLFinger14
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger21
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger21/Base HumanLFinger22
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger21/Base HumanLFinger22/Base HumanLFinger23
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger21/Base HumanLFinger22/Base HumanLFinger23/Base
      HumanLFinger24
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger31
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger31/Base HumanLFinger32
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger31/Base HumanLFinger32/Base HumanLFinger33
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanLCollarbone/Base HumanLUpperarm/Base HumanLForearm/Base
      HumanLPalm/Base HumanLFinger31/Base HumanLFinger32/Base HumanLFinger33/Base
      HumanLFinger34
    m_Weight: 1
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck/Base HumanHead
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanNeck/Base HumanHead/Base HumanHeadBone001
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger11
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger11/Base HumanRFinger12
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger11/Base HumanRFinger12/Base HumanRFinger13
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger11/Base HumanRFinger12/Base HumanRFinger13/Base
      HumanRFinger14
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger21
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger21/Base HumanRFinger22
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger21/Base HumanRFinger22/Base HumanRFinger23
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger21/Base HumanRFinger22/Base HumanRFinger23/Base
      HumanRFinger24
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger31
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger31/Base HumanRFinger32
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger31/Base HumanRFinger32/Base HumanRFinger33
    m_Weight: 0
  - m_Path: Base HumanPelvis/Base HumanSpine1/Base HumanSpine2/Base HumanSpine3/Base
      HumanRibcage/Base HumanRCollarbone/Base HumanRUpperarm/Base HumanRForearm/Base
      HumanRPalm/Base HumanRFinger31/Base HumanRFinger32/Base HumanRFinger33/Base
      HumanRFinger34
    m_Weight: 0
  - m_Path: Base HumanPelvisGizmo
    m_Weight: 0
  - m_Path: Base HumanRCollarboneGizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger11Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger12Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger13Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger21Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger21GizmoGizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger22Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger22GizmoGizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger23Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger31Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger32Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFinger33Gizmo
    m_Weight: 0
  - m_Path: Base HumanRFootGizmo
    m_Weight: 0
  - m_Path: Base HumanRForearmGizmo
    m_Weight: 0
  - m_Path: Base HumanRibcageGizmo
    m_Weight: 0
  - m_Path: Base HumanRPalmGizmo
    m_Weight: 0
  - m_Path: Base HumanRPlatform
    m_Weight: 0
  - m_Path: Base HumanRToe11Gizmo
    m_Weight: 0
  - m_Path: Base HumanRUpperarmGizmo
    m_Weight: 0
  - m_Path: Base HumanSpine1Gizmo
    m_Weight: 0
  - m_Path: Base HumanSpine2Gizmo
    m_Weight: 0
  - m_Path: torso_geo
    m_Weight: 0
