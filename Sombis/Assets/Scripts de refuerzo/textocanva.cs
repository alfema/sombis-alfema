﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textocanva : MonoBehaviour
{
    bool cuenta;
    float contador;
    public Text disparosText, powerupText, enemyText, ammotext, dietext, vidatext, tiempotext;
    // Start is called before the first frame update
    void Start()
    {
        cuenta = true;
    }

    // Update is called once per frame
    void Update()
    {
        //esto son los textos que tengo puestos en el propio canvas 
        if (cuenta)
        {
        contador = Time.time;
        }
        disparosText.text = "Te quedan " + Movimiento.contador_disparos + " disparos.";
        powerupText.text = "Tienes " + Movimiento.cont_powerup + " power ups.";
        enemyText.text = "Has matado a " + enemieç.cont_enemy + " enemigos.";
        vidatext.text = Movimiento.vidachar.ToString();
        ammotext.text = Movimiento.ammo + " / " + Movimiento.contador_disparos;

        // aqui tengo que cuando no le queden vidas al personaje se active este if para que muestre estos mensajes
        if(Movimiento.vidachar <= 0)
        {
            Movimiento.ammo = 0;
            dietext.text = "Has perdido";
            tiempotext.text = "Has aguantado " + contador;
            cuenta = false;
        }
    }
}
