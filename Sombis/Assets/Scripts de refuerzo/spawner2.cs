﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner2 : MonoBehaviour
{
    float timeaux;
    public GameObject spawn, enemy;
    // Start is called before the first frame update
    void Start()
    {
        timeaux = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //aqui tengo el otro spawner
        float timedif = Time.time - timeaux;
        if (timedif > 10f)
        {
            Vector3 pos = new Vector3(spawn.transform.position.x, spawn.transform.position.y - 1f, spawn.transform.position.z);
            GameObject clone = Instantiate(enemy, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
            timeaux = Time.time;
        }
    }
}