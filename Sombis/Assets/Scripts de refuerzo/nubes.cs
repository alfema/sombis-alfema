﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nubes : MonoBehaviour
{
    public bool actibable1, actibable2, actibable3;
    float velocidad = 1f;
    public GameObject nube1, nube2, nube3, target1, target2, target3;
    Vector3 vnub1, vnub2, vnub3;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = true;
        Time.timeScale = 1f;
        actibable1 = true;
        actibable2 = true;
        actibable3 = true;
        vnub1 = nube1.transform.position;
        vnub2 = nube2.transform.position;
        vnub3 = nube3.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
       
        if(Vector3.Distance(nube1.transform.position, target1.transform.position) < 0.5f)
        {
            actibable1 = false;
        }
        if(Vector3.Distance(nube1.transform.position, vnub1) < 0.5f)
        {
            actibable1 = true;
        }
        if(actibable1 == true)
        {
            nube1.transform.position = Vector3.MoveTowards(nube1.transform.position, target1.transform.position, velocidad * Time.deltaTime);
        }
        if (actibable1 == false)
        {
            nube1.transform.position = Vector3.MoveTowards(nube1.transform.position, vnub1, velocidad * Time.deltaTime);
        }

        if (Vector3.Distance(nube2.transform.position, target2.transform.position) < 0.5f)
        {
            actibable2 = false;
        }
        if (Vector3.Distance(nube2.transform.position, vnub2) < 0.5f)
        {
            actibable2 = true;
        }

        if (actibable2 == true)
        {
            nube2.transform.position = Vector3.MoveTowards(nube2.transform.position, target2.transform.position, velocidad * Time.deltaTime);
        }
        if (actibable2 == false)
        {
            nube2.transform.position = Vector3.MoveTowards(nube2.transform.position, vnub2, velocidad * Time.deltaTime);
        }

        if (Vector3.Distance(nube3.transform.position, target3.transform.position) < 0.5f)
        {
            actibable3 = false;
        }
        if (Vector3.Distance(nube3.transform.position, vnub3) < 0.5f)
        {
            actibable3 = true;
        }

        if (actibable3 == true)
        {
            nube3.transform.position = Vector3.MoveTowards(nube3.transform.position, target3.transform.position, velocidad * Time.deltaTime);
        }
        if (actibable3 == false)
        {
            nube3.transform.position = Vector3.MoveTowards(nube3.transform.position, vnub3, velocidad * Time.deltaTime);
        }


        //esto es un script el cual he hecho para hacer que unas nubes se movieran hacia delante y hacia atras, se activan y desactivan cuando llegan a sus destinos 
        // cuando llega al punto A va hacia al punto B y se resetea para que hagan el mismo movimiento continuo a través de un MoveTowards

    }
}
