﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovDisparo : MonoBehaviour
{
    float velocidad;
    Rigidbody disparo;
    // Start is called before the first frame update
    void Start()
    {
        disparo = GetComponent<Rigidbody>();
        //aquí hace que rote hacia donde estoy mirando yo cambiando mi rotación a la suya 
        transform.rotation = Movimiento.rota;
        velocidad = 20f;
    }

    // Update is called once per frame
    void Update()
    {
        //movimiento disparo hacia delante
        Vector3 adelante = transform.TransformDirection(Vector3.forward);
        disparo.MovePosition(disparo.position + adelante * velocidad * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        //destrucción de la bala cuando choca con un muro
        if(collision.gameObject.tag == "wall")
        {
            Destroy(gameObject);
        }
    }

}
