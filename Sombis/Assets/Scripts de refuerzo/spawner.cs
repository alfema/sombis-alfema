﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    float timeaux;
    public GameObject spawn, enemy, goalObject, clone;
    // Start is called before the first frame update
    void Start()
    {
        timeaux = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //es el spawner de enemigos 1 el cual son isntancias como las balas va cada 10 segundos
        float timedif = Time.time - timeaux;
        if (timedif > 10f)
        {
            Vector3 pos = new Vector3(spawn.transform.position.x, spawn.transform.position.y -1f, spawn.transform.position.z);
            //Vector3 rot = new Vector3(Character.transform.rotation.x, Character.transform.rotation.y, Character.transform.position.z);
            clone = Instantiate(enemy, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
            timeaux = Time.time;
        }
    }
}
