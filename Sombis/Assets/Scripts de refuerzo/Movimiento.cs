﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public static int vidachar;

    public int vidamax;

    public float velocidad, timeaux;

    static public float cont_powerup = 0, contador_disparos, ammo;

    static public Quaternion rota;

    public GameObject bola, my_char, spawner;

    static public Rigidbody Character;

    RaycastHit hit;

    Vector3 direccion = Vector3.zero, ray;
    public static Vector3 poschar;

    public static Renderer rend;

    public Animator cameranim, anim;
    // Start is called before the first frame update
    void Start()
    {

        my_char = GetComponent<GameObject>();
        Character = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();

        vidamax = 3;
        contador_disparos = 50f;
        velocidad = 5.0f;
        timeaux = 0.6f;
        ammo = 5f;
        vidachar = vidamax;

        rend.material.color = Color.green;

    }

    // Update is called once per frame
    void Update()
    {

        //Mi posición que la utilizo para otras cosas como calcular la distancia mia y el enemigo
        poschar = this.transform.position;


        //Esto es el disparo el cual le he puesto una animación de recarga y un limite de disparos para que pueda recargar, esta recarga es de 5 en 5.
        //esta bala es una instancia la cual recoge mi rotación a través de un quaternion y luego se lo aplico al disparo, y le quito uno de munición de recarga
        float timedif = Time.time - timeaux;
        if (Input.GetKeyDown(KeyCode.F) && timedif > 0.5f && contador_disparos > 0f && ammo > 0 )
        { 
            Vector3 pos = new Vector3(spawner.transform.position.x , spawner.transform.position.y , spawner.transform.position.z);
            GameObject clone = Instantiate(bola, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
            timeaux = Time.time;
            contador_disparos -= 1f;
            rota = this.transform.localRotation;
            ammo--;
        }
        //Aquí pongo la animación para que pueda recargar que es la de la pistola
        if (Input.GetKeyDown(KeyCode.R))
        {
            anim.SetBool("do", true);
            timedif = -2f;
            ammo = 5f;
        }
        else
        {
            anim.SetBool("do", false);
        }
        //Aquí le digo que si es mayor que 5 la munición que hay dentro de la pistola que no pueda llegar a ser más
        if(ammo > 5)
        {
            ammo = 5f;
        }
        //Ray cast creando un collider hit he detectado que haya un enemigo y bajo el raycast un 0.1 para que pueda detectar a las pelotas porque son bajitas
        ray = transform.TransformDirection((Vector3.forward) - new Vector3(0,0.1f,0));

        Debug.DrawRay(transform.position, ray * 10, Color.red);
        
        if (Physics.Raycast(transform.position, ray, out hit))
        {
            if(hit.collider.tag == "enemy" || hit.collider.tag == "enemy2")
            {
                Activate.activar = true;
            }
            else
            {
                Activate.activar = false;
            }
        }




        //movimiento del personaje
        if (Input.GetAxis("Vertical") > 0)
        {
            //movimiento delante
            Vector3 fwd = transform.TransformDirection(Vector3.forward);
            Character.MovePosition(Character.position + fwd * velocidad * Time.deltaTime);
            cameranim.SetBool("mov", true);
        }
        else
        {
            cameranim.SetBool("mov", false);
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            //movimiento atras
            Vector3 back = transform.TransformDirection(Vector3.back);

            Character.MovePosition(Character.position + back * velocidad * Time.deltaTime);
            cameranim.SetBool("mov", true);
        }

        //movimiento girar
        if (Input.GetAxis("Horizontal") > 0)
        {
            Character.transform.Rotate(0,1,0);
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            Character.transform.Rotate(0, -1, 0);
        }
        else
        {
            Character.transform.Rotate(0,0,0);
        }
        //PowerUp uso del mismo
        if (Input.GetKeyDown(KeyCode.G))
        {
            cont_powerup--;
        }
        if(cont_powerup <= 0)
        {
            cont_powerup = 0;
        }
    }
    //PowerUp cuando choque con el mismo
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "PowerUp")
        {
            Destroy(collision.gameObject);
            cont_powerup++;
        }
    }

}
