﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ammo : MonoBehaviour
{
    float cuenta;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {

        //He hecho un script en el cual el enemigo suelta un power up de munición el cual te la recarga, por ende no he hecho que termine la partida 
        //cuando te quedas sin balas solamente cuando mueres.
        if(collision.gameObject.tag == "player")
        {
            cuenta = Random.Range(4, 6);
            Movimiento.contador_disparos += cuenta;
            Destroy(gameObject);
        }
    }
}
