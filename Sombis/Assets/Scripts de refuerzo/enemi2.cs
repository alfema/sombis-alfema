﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemi2 : MonoBehaviour
{
    public int random;

    public static bool destroy;

    public float vidas;

    public GameObject caja, yo, mun;

    public NavMeshAgent agent;
    Renderer rend;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        vidas = 3;
        rend = GetComponent<Renderer>();
        rend.material.color = Color.green;
    }

    void Update()
    {
        //Que siga el personaje principal esquivando todo bakeandolo
        agent.SetDestination(yo.transform.position);
        
        //Vida del enemigo y su cambio de color
        if(vidas == 2)
        {
            rend.material.color = Color.yellow;
        }
        if(vidas == 1)
        {
            rend.material.color = Color.red;
        }
        if(vidas <= 0)
        {

            enemieç.cont_enemy++;
            random = Random.Range(0, 10);
            if (random == 0 || random == 4 || random == 7)
            {
                //esto es el powerup de la munición que puede que te la suelte a través de un random.range
                Vector3 pos = this.transform.position;
                GameObject clone = Instantiate(mun, pos, Quaternion.identity) as GameObject;
                clone.SetActive(true);
            }
            Destroy(this.gameObject);
        }
        //Si el enemigo choca conmigo y me quita vida se destruye automaticamente para que no siga molestando
        if(destroy == true)
        {
            Destroy(gameObject);
            destroy = false;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        //colision de la bala con el enemigo 
        if(collision.gameObject.tag == "bala")
        {
            vidas--;
            Destroy(collision.gameObject);
        }
        //colision con el personaje y al resta de la vida del personaje y su destrucción
        if (collision.gameObject.tag == "player")
        {
            Movimiento.vidachar--;
            destroy = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        //Esto es el rango el cual cuando este dentro del rango creado en el personaje (el cual es un empty) se le restara la vida
        if (other.gameObject.tag == "rango" && Input.GetKeyDown(KeyCode.G) && Movimiento.cont_powerup > 0f)
        {
            vidas--;
            Movimiento.cont_powerup--;
        }
        //Le he aplicado al personaje y al empty rango un cubo al cual le he puesto un color y cuando detecta que hay un enemigo dentro se activa
        if (other.gameObject.tag == "rango")
        {
            caja.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "rango")
        {
            caja.SetActive(false);
        }
    }
}

