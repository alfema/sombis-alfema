﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class enemieç : MonoBehaviour
{
    public int random;

    public float vida_enemigo, velocidad;

    static public bool destroyi;
    static public float cont_enemy;

    static public Rigidbody rigid_enemy;

    public GameObject target, caja, mun;

    Renderer rend;

     public Vector3 pos_enemy;
    // Start is called before the first frame update
    void Start()
    {
        rigid_enemy = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();

        vida_enemigo = 2;
        velocidad = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //posiciond del personaje
        pos_enemy = this.transform.position;

        //movimiento hacia el personaje principal
        float movimiento = velocidad * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, movimiento);

        //cambia de color al enemigo
        if(vida_enemigo == 2)
        {
            rend.material.color = Color.black;
        }
        if (vida_enemigo == 1)
        {
            rend.material.color = Color.white;
        }
        if(vida_enemigo <= 0)
        {
            cont_enemy++;
            random = Random.Range(0, 10);
            if(random == 0 || random == 4 || random == 7)
            {
            Vector3 pos = this.transform.position;
            GameObject clone = Instantiate(mun, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
            }
            Destroy(gameObject);
        }
        if (destroyi)
        {
            Destroy(gameObject);
            destroyi = false;
        }

        //cambia de color al personaje
        if (Vector3.Distance(pos_enemy, Movimiento.poschar) < 5f)
        {
            Movimiento.rend.material.color = Color.yellow;
            if (Vector3.Distance(pos_enemy, Movimiento.poschar) < 3f)
            {
                Movimiento.rend.material.color = Color.red;
            }
        }
        else
        {
            Movimiento.rend.material.color = Color.green;
        }
    }
        //quitar vida del enemigo
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "bala")
        {
            Destroy(collision.gameObject);
            vida_enemigo--;
        }
        if(collision.gameObject.tag == "player")
        {
            Movimiento.vidachar--;
            destroyi = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "rango" && Input.GetKeyDown(KeyCode.G) && Movimiento.cont_powerup > 0f)
        {
            vida_enemigo--;
        }
        if(other.gameObject.tag == "rango")
        {
            caja.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "rango")
        {
            caja.SetActive(false);
        }
    }
}
