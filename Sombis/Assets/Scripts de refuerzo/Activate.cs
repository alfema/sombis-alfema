﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Activate : MonoBehaviour
{
    public static bool activar;

    public GameObject kk;
    // Start is called before the first frame update
    void Start()
    {
        activar = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Esto es el script asociado a la RawImage de la mirilla, cuando detecta el raycast se activa
        if (activar)
        {
            kk.SetActive(true);
        }
        else
        {
            kk.SetActive(false);
        }
    }
}
