﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public class AudioManager : MonoBehaviour
{
    static public float volumen;
    AudioMixer audioMixer;
    public Sound[] sounds;
    //este es un sistema de sonido que he creado a raiz de entender un par de videos en el cual desde un solo gameobject puede funcionar casi todos
    //los sonidos
    void Awake()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }
    private void Update()
    {
 
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }
    public void auidox (float volume)
    {
        volumen = volume;
    }
}

