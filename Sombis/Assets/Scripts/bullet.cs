﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    float cuenta;
    float velocidad;
    Rigidbody disparo;
    public Quaternion uzi , si;
    public GameObject uzi1;
    Vector3 chance;

    public float min, max;
    // Esto es el script de la bala para que pueda ir hacia donde este mirando el arma 
    void Start()
    {


        disparo = GetComponent<Rigidbody>();

        transform.rotation = Character.rotacion;
        velocidad = 30f;

        min = Random.Range(-0.05f, 0.05f);
        max = Random.Range(-0.05f, 0.05f);
    }
    // Update is called once per frame
    void Update()
    {

        cuenta += 0.01f;
        if(cuenta >= 5f)
        {
            Destroy(gameObject);
            cuenta = 0;
        }
        if (Input.GetKey(KeyCode.Mouse1))
        {
            Vector3 adelante1 = transform.TransformDirection(Vector3.left);
            disparo.MovePosition(disparo.position + adelante1 * velocidad * Time.deltaTime);
        }
        else
        {
            Vector3 adelante = transform.TransformDirection(new Vector3(-1, min, max));
            disparo.MovePosition(disparo.position + adelante * velocidad * Time.deltaTime);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
            Destroy(gameObject , 2f);
    }
}
