﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class minimapa : MonoBehaviour
{
    public GameObject mini;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Esta es una de las parte de I+D que he querido implementar que es el minimapa, en este minimapa te sigue un icono para saber quien eres continuamente.
    void Update()
    {
        float movimiento = Character.velocity * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position,new Vector3(mini.transform.position.x, this.transform.position.y, mini.transform.position.z), movimiento);
    }
}
