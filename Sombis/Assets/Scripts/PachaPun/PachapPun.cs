﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PachapPun : MonoBehaviour
{
    public GameObject sprite;
    // Esto es el sistema de mejora de armas, por un precio te mejora
    void Start()
    {
        sprite.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && Puntuacion.puntuacion > 4000)
        {
            Character.dañohead *= 2;
            Character.daño *= 2;
            Puntuacion.puntuacion -= 4000;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "char")
        {
            sprite.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "char")
        {
            sprite.SetActive(false);
        }
    }
}
