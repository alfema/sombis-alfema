﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovCamara : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    //Este es un script que tiene la camara para que pueda rotar al rededor mia junto el personaje
    void Update()
    {
        float newRotationY = transform.localEulerAngles.y - Input.GetAxis("Mouse X") * Character.sensibilidad * Time.deltaTime;
        float newRotationX = transform.localEulerAngles.x - Input.GetAxis("Mouse Y") * Character.sensibilidad * Time.deltaTime;
        gameObject.transform.localEulerAngles = new Vector3(newRotationX, 0, 0);

      
    }
}
