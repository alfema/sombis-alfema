﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuerteInsta : MonoBehaviour
{

    float timedif = 10, timediff = 20;
    //Este es el script donde todos los power ups estan hechos
    void Update()
    {
        if (Character.PowerUp1 == true)
        {
            

            timedif -= Time.deltaTime;

            InstantKill();
            if(timedif < 0)
            {
                Character.daño = 50;
                timedif = 10;
                Character.PowerUp1 = false;
            } 
        }
        else
        {
            Character.daño = 50;
        }
        if(Character.PowerUp2 == true)
        {
            FullAmmo();
            Character.PowerUp2 = false;
        }
        if(Character.PowerUp3 == true && Vidachar.vida < 3)
        {
            Vidachar.vida++;
            Character.PowerUp3 = false;
        }
        if(Character.PowerUp4 == true)
        {
            Enemigo.puntuacioncuer *= 2;
            Enemigo.puntmuerte *= 2;
            if (timediff < 0)
            {
                Enemigo.puntmuerte = 100;
                Enemigo.puntuacioncuer = 10;
                timediff = 20;
                Character.PowerUp4 = false;
            }
        }
    }
    void InstantKill()
    {
        Character.daño = Enemigo.vida_max_ene;
    }
    void FullAmmo()
    {
        Character.max_ammo = 300;
    }
}
