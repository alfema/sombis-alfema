﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PuntuacionEnd : MonoBehaviour
{
    public TextMeshProUGUI punt;

    // Este script es para poder enseñar la puntuacion final en la última pantalla (la de muerte)
    void Start()
    {
        Cursor.lockState= CursorLockMode.None;
        Cursor.visible = true;
        punt = gameObject.GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        punt.text = "Final score =" + Puntuacion.puntuacion.ToString();
    }
}
