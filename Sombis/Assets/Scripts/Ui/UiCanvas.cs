﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiCanvas : MonoBehaviour
{
    public GameObject title, control;


    // En este script tengo puesto que el cursor no se quede en medio y se puede ver
    void Update()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void quitar()
    {
       title.SetActive(false);
       control.SetActive(true);
    }
    public void poner()
    {
        title.SetActive(true);
        control.SetActive(false);
    }
}
