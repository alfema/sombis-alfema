﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
public class Inicio : MonoBehaviour
{
    public AudioMixer audioMixer;

    Animator change;
    public Sound[] sounds;
    // Este scirpt son las funciones que tiene el menu de inicio, este desde que sea full screen, 
    //que puedas cambiar el volumen del juego y que puedas salir del juego. Junto a las animaciones del propio menú
    void Start()
    {
        change = GetComponent<Animator>();
    }


    void Update()
    {
       
    }
    public void Iniciodo()
    {
        SceneManager.LoadScene(1);
    }
    public void optionsChange()
    {
        change.SetBool("change", true);
    }
    public void SetVolume(float volume)
    {
        foreach (Sound s in sounds)
        {
            audioMixer.SetFloat("volume", s.volume);
        }
    }
    public void SetFullScreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
    public void Backto()
    {
        change.SetBool("change", false);
    }
    public void quit1()
    {
        Application.Quit();
    }
}
