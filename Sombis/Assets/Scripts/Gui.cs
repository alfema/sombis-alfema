﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Gui : MonoBehaviour
{

    public TextMeshProUGUI ammo;
    // Start is called before the first frame update
    void Start()
    {
        ammo = gameObject.GetComponent<TextMeshProUGUI>();
    }

    // Este script es para que pueda decirte la GUI cuanta munición tienes o te queda
    void Update()
    {
        ammo.text = Character.municion + " / " + Character.max_ammo;
    }
}
