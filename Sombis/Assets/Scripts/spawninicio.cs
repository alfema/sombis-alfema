﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawninicio : MonoBehaviour
{
    public GameObject spawn, enemy;
    float timeaux;
    // Start is called before the first frame update
    void Start()
    {

        timeaux = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        //Este es el spawner que utilizo en los dos gameobjects que hacen aparecer los enemigos, este spawner es del inicio po rlo tanto es diferente a los spawners normales que hay en la escena del juego
        float timedif = Random.Range(0, 200);
        if (timedif == 17)
        {
            Vector3 pos = new Vector3(spawn.transform.position.x, spawn.transform.position.y, spawn.transform.position.z);
            GameObject clone = Instantiate(enemy, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
            timeaux = Time.time;
        }
    }
}
