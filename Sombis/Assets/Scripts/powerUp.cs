﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class powerUp : MonoBehaviour
{
    public float velocidad_color, contador_color_rojoazul, contador_color_verdeazul, contador_color_verdeamarillo, contador_color_amarillorojo;
    Color lerpedColor = Color.red;

    private Renderer rend;
    Quaternion rotation;
    Rigidbody powerup;
    float timer = 0;

    public Animation anim;
    // ESTE SCRIPT LO HE REUTILIZADO DE LA PRACTICA QUE TENGO, HE HECHO UN PAR DE MODIFICACIONES PARA QUE NO FUERA IGUAL Y PUDIERA QUEDAR BIEN CON MI JUEGO
    void Start()
    {
        powerup = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
        anim = GetComponent<Animation>();

        velocidad_color = 0.5f;
        timer = Time.time;
    }

    void Update()
    {   
        //He decidido ponerle un movimiento hacia arriba y abajo al power up para que quedara más estético
        Vector3 down = new Vector3(transform.position.x, 7.5f, transform.position.z);
        Vector3 up = new Vector3(transform.position.x, 7f, transform.position.z);

        float lerp = Mathf.PingPong(Time.time, 3.0f) / 3.0f;
        transform.position = Vector3.Lerp(down, up, lerp);
        
        //Aquí hago que gire mediante el transform Rotate
        powerup.transform.Rotate(0, -1, 0);
    }

    void Rojoazul()
    {
        //Aquí he creado un void al cual le he llamado Rojoazul y lo que hace es un lerp entre colores junto con un material aplicado en HDR para que se vea
        //más luminoso, pd me gustaría aprender sobre programacion en tema de shaders si es posible y tienes algunos tutoriales o sabes de ellos
        //me gustaría verlos. Gracias!!
        contador_color_rojoazul +=(velocidad_color * 0.1f);

        float lerp = Mathf.PingPong(Time.time, 3.0f) / 3.0f;
        rend.material.color = Color.Lerp(Color.red, Color.blue, contador_color_rojoazul);
        if (contador_color_rojoazul > 1f)
        {
            contador_color_verdeazul += (velocidad_color * 0.1f);
            rend.material.color = Color.Lerp(Color.blue, Color.green, contador_color_verdeazul);
            if(contador_color_verdeazul > 1f)
            {
                contador_color_verdeamarillo += (velocidad_color * 0.1f);
                rend.material.color = Color.Lerp(Color.green, Color.yellow, contador_color_verdeamarillo);
                if(contador_color_verdeamarillo > 1)
                {
                    contador_color_amarillorojo += (velocidad_color * 0.1f);
                    rend.material.color = Color.Lerp(Color.yellow, Color.red, contador_color_amarillorojo);
                    if(contador_color_amarillorojo > 1)
                    {
                        contador_color_amarillorojo = 0;
                        contador_color_rojoazul = 0;
                        contador_color_verdeamarillo = 0;
                        contador_color_verdeazul = 0;
                    }
                }
            }
        }
    }
}
