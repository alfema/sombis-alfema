﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recargar : MonoBehaviour
{
    Animator recarga;


    //Aquí tengo el script de que pueda recargar y haga la animacion que tiene el personaje 
    void Start()
    {
        recarga = GetComponent<Animator>();
    }
    void Update()
    {
        if(Character.recargara == true)
        {
            recarga.SetBool("recargando", true);
        }
    }
    void endit()
    {
        recarga.SetBool("recargando", false);
        Character.recargara = false;
    }
}
