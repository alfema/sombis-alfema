﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Puntuacion : MonoBehaviour
{
    static public float puntuacion;
    public TextMeshProUGUI punt;
    // Aqui recoge y muestra la puntuación en pantalla
    void Start()
    {
        puntuacion = 0;
        punt = gameObject.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        punt.text = puntuacion.ToString();
    }
}
