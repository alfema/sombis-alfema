﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public static int ronda, ronda_max;
    float timeaux;
    public GameObject spawn, enemy;
    // Este es el spanwer principal del juego que uso en la escena del juego, es diferente al resto porque cuenta las rondas que hay y demás
    void Start()
    {
        timeaux = 1f;
        ronda_max = Random.Range(4, 7);
        ronda = ronda_max;
    }
    void Update()
    {
        float timedif = Time.time - timeaux;
        if (timedif > 2f && ronda > 0)
        {
            Vector3 pos = new Vector3(spawn.transform.position.x, spawn.transform.position.y, spawn.transform.position.z);
            GameObject clone = Instantiate(enemy, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
            timeaux = Time.time;
            ronda -= 1;
        }
    }
}
