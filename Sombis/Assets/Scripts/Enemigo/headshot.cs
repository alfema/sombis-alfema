﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class headshot : MonoBehaviour
{
// Como parte de I+D he puesto que detecte un collider de la cabeza la bala para que pueda hacerle más daño
// este script le dice al script padre del enemigo cuando ocurre esa colision
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bala")
        {
            transform.parent.GetComponent<Enemigo>().CollisionDetected(this);
        }
    }
}
