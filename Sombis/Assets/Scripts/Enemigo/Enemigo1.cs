﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo1 : MonoBehaviour
{
    public bool yes;
    static public float range,timedif, timedif2;

    Animator eneanim;
    Vector3 delante;
    // Este script es el mismo que el primero pero se usa solo en el inicio
    void Start()
    {
        yes = false;
        timedif2 = 2;
        timedif = 1f;
        eneanim = GetComponent<Animator>();
        range = 2;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Vector3 ray = transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), ray * 2, Color.blue);
        if (Physics.Raycast(new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), ray * 2, out hit, range))
        {
            if (hit.collider.tag == "char")
            {
                eneanim.SetBool("ataca", true);
                timedif -= Time.deltaTime;
                if(timedif < 0)
                {
                    if(Vidachar.vida == 3)
                    {
                        Vidachar.vida -= 1;
                    }
                    yes = true;

                    timedif = 1;
                }
                if(yes == true)
                {
                    timedif2 -= Time.deltaTime;
                    if (timedif2 <= 0)
                    {
                        if (Vidachar.vida == 2)
                        {
                            Debug.Log("si");
                            timedif2 = 2f;
                            Vidachar.vida = 1;
                        }
                        if (timedif2 <= 0)
                        {
                            if (Vidachar.vida == 1)
                            {
                            timedif2 = 2f;
                            Vidachar.vida = 0;
                            }
                        }
                    }
                }
            }
            else
            {
                eneanim.SetBool("ataca", false);
                timedif = 1f;
            }
        }


    }

}