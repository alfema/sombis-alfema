﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class maininicio : MonoBehaviour
{
    NavMeshAgent miNavMesh;
    public static int velenem, vida_max_ene, contad_rond = 0, laronda, puntuacioncuer, puntmuerte;
    public int vida_ene, range;
    public float timer, cuenta1, cuenta2;
    float timedif3 = 1.5f;

    public Vector3 Nueva, Nueva1;
    public Transform posy;
    public Animator eneanim;

    public GameObject mychar, cabeza, yo, final;
    public GameObject instakill, ammomax, x2, vidaplus;
    Vector3 pos;


    public Transform charac;

    private Quaternion desiredRotation;
    private float rotationSpeed = 30f;


    // Start is called before the first frame update
    void Start()
    {

        miNavMesh = GetComponent<NavMeshAgent>();
        puntmuerte = 100;
        puntuacioncuer = 10;
        vida_max_ene = 100 * laronda;
        vida_ene = 100;
        velenem = 5;
        range = 2;
        timer = 1;
        pos = new Vector3(yo.transform.position.x, this.transform.position.y - 1f, this.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (vida_ene > 0)
        {
            desiredRotation = Quaternion.LookRotation(charac.transform.position - transform.position);
            transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, Time.deltaTime * rotationSpeed);
        }



        timer -= Time.deltaTime;
        if (timer < 0)
        {
            cuenta1 = Random.Range(-2f, 2f);
            cuenta2 = Random.Range(-2f, 2f);
            timer = 0f;

        }

        miNavMesh.SetDestination(new Vector3(final.transform.position.x, final.transform.position.y + 0.5f, final.transform.position.z));

        if (Vector3.Distance(yo.transform.position, final.transform.position) <= 4f)
        {
            Nueva = new Vector3(final.transform.position.x + cuenta1, final.transform.position.y + 0.5f, final.transform.position.z + cuenta2);

            miNavMesh.SetDestination(Nueva);
        }
        if (vida_ene <= 0)
        {

            muerto();
        }
        if (contad_rond == Spawner.ronda_max)
        {
            Spawner.ronda_max = Spawner.ronda_max + Random.Range(0, 6);
            contad_rond = 0;
            laronda++;
            Spawner.ronda = Spawner.ronda_max;
        }
        Debug.Log(contad_rond);

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bala")
        {
            Destroy(collision.gameObject, 0.5f);
            vida_ene -= Character.daño;
            Puntuacion.puntuacion += puntuacioncuer;
        }
    }
    public void CollisionDetected(headshot headshot)
    {
        vida_ene -= Character.dañohead;
    }
    void ataque()
    {
        Vidachar.vida -= 1;

    }
    void powerUp()
    {
        int numrandom = Random.Range(0, 500);
        if (numrandom > 10 && numrandom < 20)
        {
            GameObject clone = Instantiate(instakill, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
        }
        if (numrandom > 50 && numrandom < 60)
        {
            GameObject clone = Instantiate(x2, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
        }
        if (numrandom > 80 && numrandom < 100)
        {
            GameObject clone = Instantiate(ammomax, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
        }
        if (numrandom < 10)
        {
            GameObject clone = Instantiate(vidaplus, pos, Quaternion.identity) as GameObject;
            clone.SetActive(true);
        }
    }
    public void muerto()
    {
        eneanim.SetBool("muerto", true);
        Nueva1 = yo.transform.position;
        miNavMesh.SetDestination(Nueva1);
        timedif3 -= Time.deltaTime;
        if (timedif3 < 0)
        {
            Puntuacion.puntuacion += puntmuerte;
            contad_rond++;
            powerUp();
            Destroy(gameObject);
        }
    }
}
