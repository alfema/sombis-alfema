﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoomCamera : MonoBehaviour
{
    static public bool zoom;


    void Update()
    {
        //Aquí tengo puesto que cuando presione el click boton derecho cambie la sensibilidad para que vaya más lenta la camara y la velocidad baje también
        if (Input.GetKey(KeyCode.Mouse1))
        {
            Character.sensibilidad = 50;
            zoom = true;
        }
        else
        {
            Character.sensibilidad = 100;
            zoom = false;
        }
    }
}
