﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Character : MonoBehaviour
{
    public GameObject pausemenuUi, resumeUI;
    public static bool GameIsPaused = false;
    public bool Cursor1;
    public static bool recargara, PowerUp1, PowerUp2,PowerUp3,PowerUp4;

    public static float sensibilidad;
    public float timeaux,timeammo;

    public static int velocity, municion, max_ammo,daño, dañohead, puntuacion;
    public int fuerzaSalto;

    public Animator anim;
    Rigidbody character;

    public static Quaternion rotacion;
    Vector3 pos;

    public GameObject bullet, disparador;

    public Transform uzi;

    public ParticleSystem muzzle;
    //Este es el script del personaje en el cual tiene el movimiento del personaje, el disparo, la sensibilidad de la camara, 
    //el que pare el juego cuando lo quieras pausar, el que pueda salir del juego con la M cuando quierda
    //los trigger con los powerups para poder activarlos y el sistema de municion y cantidad de municion maxima
    void Start()
    {
        FindObjectOfType<AudioManager>().Play("BackgroundM");
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        Enemigo.laronda = 1;


        character = GetComponent<Rigidbody>();

        fuerzaSalto = 5;
        sensibilidad = 100f;
        timeaux = 0;
        municion = 30;
        recargara = false;
        max_ammo = 300;
        daño = 50;
        dañohead = 100;
        timeammo = 10;
        Time.timeScale = 1f;
    }
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.M))
        {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene(0);
        }
        //parar juego
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused == false)
            {
                Cursor.lockState = CursorLockMode.None;
                Time.timeScale = 0.0000000001f;
                GameIsPaused = true;
                pausemenuUi.SetActive(true);
                resumeUI.SetActive(false);
                Cursor.visible = true;

            }
            else
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                pausemenuUi.SetActive(false);
                Time.timeScale = 1f;
                GameIsPaused = false;
                resumeUI.SetActive(true);
            }
        }
        //caminado
        rotacion =uzi.rotation;
        if (Input.GetKey(KeyCode.LeftShift))
        {

            if (Input.GetAxisRaw("Vertical") > 0)
            {
                Vector3 fwd = transform.TransformDirection(Vector3.forward);
                character.MovePosition(character.position + fwd * velocity * Time.deltaTime);
            }
            if (Input.GetAxisRaw("Vertical") < 0)
            {
                Vector3 bck = transform.TransformDirection(Vector3.back);
                character.MovePosition(character.position + bck * velocity * Time.deltaTime);
            }
        }
        else
        {

            if (Input.GetAxisRaw("Vertical") > 0)
            {
                Vector3 fwd = transform.TransformDirection(Vector3.forward);
                character.MovePosition(character.position + fwd * velocity * Time.deltaTime);
            }
            if (Input.GetAxisRaw("Vertical") < 0)
            {
                Vector3 bck = transform.TransformDirection(Vector3.back);
                character.MovePosition(character.position + bck * velocity * Time.deltaTime);
            }
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                Vector3 fwd = transform.TransformDirection(Vector3.right);
                character.MovePosition(character.position + fwd * velocity * Time.deltaTime);
            }
            if (Input.GetAxisRaw("Horizontal") < 0)
            {
                Vector3 bck = transform.TransformDirection(Vector3.left);
                character.MovePosition(character.position + bck * velocity * Time.deltaTime);
            }
        }
        else
        {
            if (Input.GetAxisRaw("Horizontal") > 0)
            {
                Vector3 fwd = transform.TransformDirection(Vector3.right);
                character.MovePosition(character.position + fwd * velocity * Time.deltaTime);
            }
            if (Input.GetAxisRaw("Horizontal") < 0)
            {
                Vector3 bck = transform.TransformDirection(Vector3.left);
                character.MovePosition(character.position + bck * velocity * Time.deltaTime);
            }
        }
        //movimiento camara

        float newRotationY = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensibilidad * Time.deltaTime;
        float newRotationX = transform.localEulerAngles.x - Input.GetAxis("Mouse Y") * sensibilidad * Time.deltaTime;

        character.transform.localEulerAngles = new Vector3(0, newRotationY, 0);
        //velocidad
        if (zoomCamera.zoom == true)
        {
            velocity = 1;
        }
        else
        {
            if (zoomCamera.zoom == true && Input.GetKey(KeyCode.LeftShift))
            {
                velocity = 1;
            }
            if(zoomCamera.zoom == false)
            {
                velocity = 0;
            }
            if(zoomCamera.zoom == false && Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            {
                velocity = 3;
            }
            if(zoomCamera.zoom == false && Input.GetKey(KeyCode.LeftShift))
            {
                velocity = 5;
            }
        }
        //animacion
        if(velocity == 0)
        {
            anim.SetBool("andar", false);
            anim.SetBool("Correr", false);
        }
        if(velocity == 2 ||velocity == 3)
        {
            anim.SetBool("andar", true);
        }
        if(velocity == 5)
        {
            anim.SetBool("Correr", true);
        }
        if(velocity < 5)
        {
            anim.SetBool("Correr", false);
        }

        //spawner y municion

        float timedif = Time.time - timeaux;

        Debug.DrawRay(this.transform.position, Vector3.forward, Color.red);
                    if (timedif > 0.05f && Input.GetKey(KeyCode.Mouse0) && recargara == false && municion > 0)
                    {
                        GameObject clone = Instantiate(bullet, uzi.transform.position, Quaternion.identity) as GameObject;
                        clone.SetActive(true);
                        timeaux = Time.time;
                        municion--;
                        muzzle.Play();
                        FindObjectOfType<AudioManager>().Play("Shot");
                        if (max_ammo > 0)
                        {
                            max_ammo--;
                        }
                    }
   


        
        if(municion == 0 && max_ammo > 0)
        {
            recargar();
            municion = 30;
            if (max_ammo < 30)
            {
                municion = max_ammo;
            }
        }
        if(municion < 30 && Input.GetKeyDown(KeyCode.R) && max_ammo > 0)
        {
            if(max_ammo > 30)
            {
                recargar();
                municion = 30;
            }
            if(max_ammo < 30)
            {
                municion = max_ammo;
            }
        }
        if(municion == 0 && max_ammo == 0)
        {
            timeammo -= Time.deltaTime;
            if(timeammo < 0)
            {
                municion += 30;
                timeammo = 10;
            }
        }
    }
    void recargar()
    {
        if(max_ammo > 0)
        {
            recargara = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "1PowerUp")
        {
            PowerUp1 = true;
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag == "2PowerUp")
        {
            PowerUp2 = true;
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag == "3PowerUp")
        {
            PowerUp3 = true;
            Destroy(other.gameObject);
        }
        if (other.gameObject.tag == "4PowerUp")
        {
            PowerUp4 = true;
            Destroy(other.gameObject);
        }
    }
    public  void quitgame()
    {
        SceneManager.LoadScene(0);
    }
}
